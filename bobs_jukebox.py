from banner import banner_text
import random

import readline


bands = {
    'tool': {'albums':
                {           
                    'opiate': {'1': 'sweat', '2': 'hush', '3': 'part of me', '4': 'cold and ugly', '5': 'jerk-off', '6': 'opiate', '7': 'the gaping lotus experience', 'year': 1992},
                    'undertow': {'1': 'intolerance', '2': 'prison sex', '3': 'sober', '4': 'bottom', '5': 'crawl away', '6': 'swamp song', '7': 'undertow', '8': '4° 4 degrees', '9': 'flood', '10': 'disgustipated', 'year': 1993},
                    'aenima': {'1': 'stinkfist', '2': 'eulogy', '3': 'h.', '4': 'forty-six & 2', '5': 'message to harry manback*', '6': 'hooks with a catch', '7': 'jimmy', '8': 'die eier von satan', '9': 'pushit', '10': 'aenema', '11': 'third eye', 'year': 1996},
                    'salival': {'1': 'third eye live', '2': 'part of me live', '3': 'pushit live', '4': 'message to harry manback ii', '5': 'you lied live', '6': 'merkaba live', '7': 'no quarter', '8': 'lamc', '9': "maynard's dick", '10': 'hidden track', 'year': 2000},
                    'lateralus': {'1': 'the grudge', '2': 'the patient', '3': 'schism', '4': 'parabol', '5': 'parabola', '6': 'ticks and leeches', '7': 'lateralus', '8': 'disposition', '9': 'reflection', '10': 'faaip de oiad', 'year': 2001},
                    '10,000 days': {'1': 'vicarious', '2': 'jambi', '3': 'wings for marie pt. 1', '4': '10 000 days wings pt. 2', '5': 'the pot', '6': 'lipan conjuring', '7': 'lost keys', '8': 'rosetta stoned', '9': 'intension', '10': 'right in two', '11': 'viginti tres', 'year': 2006},
                    'fear inoculum': {'1': 'fear inoculum', '2': 'pneuma', '3': 'litanie contre la peur', '4': 'invincible', '5': 'legion inoculant', '6': 'descending', '7': 'culling voices', '8': 'chocolate chip trip', '9': '7empest', '10': 'mockingbeat', 'year': 2019}
                }
             },
    'pennywise': {'albums':
                    {
                        'self-titled': {'1': "wouldn't it be nice", "2": "rules", "3": "the secret", "4": "living for today", "5": "come out fighting", "6": "homeless", "7": "open door", "8": "pennywise", "9": "who's to blame", "10": "fun and games", "11": "kodiak", "12": "side one", "13": "no reason why", "14": "bro hymn", "year": 1991},
                        'unknown road': {'1': "unknown road", "2": "homesick", "3": "time to burn", "4": "it's up to me", "5": "you can demand", "6": "nothing", "7": "vices", "8": "city is burning", "9": "dying to know", "10": "tester", "11": "try to conform", "12": "give and get", "13": "clear your head", 'year': 1993},
                        "about time": {'1': "peaceful day", "2": "waste of time", "3": "perfect people", "4": "every single day", "5": "searching", "6": "not far away", "7": "freebase", "8": "it's what you do with it", "9": "try", "10": "same old story", "11": "i won't have it", "12": "killing time", "year": 1995},
                        "full circle": {'1': "fight till you die", "2": "date with destiny", "3": "get a life", "4": "society", "5": "final day", "6": "broken", "7": "running out of time", "8": "you'll never make it", "9": "every time", "10": "nowhere fast", "11": "what if i", "12": "go away", "13": "did you really?", "14": "bro hymn tribute", "year": 1997},
                        "straight ahead": {'1': "greed", "2": "my own country", "3": "can't believe it", "4": "victim of reality", "5": "might be a dream", "6": "still can be great", "7": "straight ahead", "8": "my own way", "9": "one voice", "10": "alien", "11": "watch me as i fall", "12": "just for you", "13": "can't take anymore", "14": "american dream", "15": "need more", "16": "never know", "17": "badge of pride", "year": 1999},
                        "land of the free?": {'1': "time marches on", "2": "land of the free?", "3": "the world", "4": "fuck authority", "5": "something wrong with me", "6": "enemy", "7": "my god", "8": "twist of fate", "9": "who's on your side", "10": "its up to you", "11": "set me free", "12": "divine intervention", "13": "wto", "14": "anyone listening", "year": 2001},
                        "from the ashes": {'1': "now i know", "2": "god save the usa", "3": "something to change", "4": "waiting", "5": "salvation", "6": "look who you are", "7": "falling down", "8": "holiday in the sun", "9": "this is only a test", "10": "punch drunk", "11": "rise up", "12": "yesterdays", "13": "change my mind", "14": "judgment day", "year": 2003},
                        "the fuse": {'1': "knocked down", "2": "yell out", "3": "competition song", "4": "take a look around", "5": "closer", "6": "6th avenue nightmare", "7": "the kids", "8": "fox tv", "9": "stand up", "10": "dying", "11": "disconnect", "12": "premeditated murder",  "13": "best i can", "14": "18 soldiers", "15": "lies", "year": 2005},
                        "reason to believe": {'1': "(intro) as long as we can", "2": "one reason", "3": "faith and hope", "4": "something to live for", "5": "all we need", "6": "the western world", "7": "we'll never know", "8": "confusion", "9": "nothing to lose", "10": "its not enough to believe", "11": "you get the life you choose", "12": "affliction",  "13": "brag, exaggerate & lie", "14": "die for you", "year": 2008},
                        "all or nothing": {'1': "all or nothing", "2": "waste another day", "3": "revolution", "4": "stand strong", "5": "let us hear your voice", "6": "seeing red", "7": "songs of sorrow", "8": "x generation", "9": "we have it all", "10": "tomorrow", "11": "all along", "12": "united", "13": "the fallen", "14": "locked in", "year": 2012},
                        "yesterdays": {'1': "what you deserve", "2": "restless time", "3": "noise pollution", "4": "violence never ending", "5": "am oi!", "6": "thanksgiving", "7": "she's a winner", "8": "slowdown", "9": "public defender", "10": "no way out", "11": "i can remember", "year": 2014},
                        "never gonna die": {'1': "never gonna die", "2": "american lies", "3": "keep moving on", "4": "live while you can", "5": "we set fire", "6": "she said", "7": "can't be ignored", "8": "goodbye bad times", "9": "a little hope", "10": "won't give up the fight", "11": "can't save you now", "12": "all the ways u can die", "13": "listen", "14": "something new", "year": 2018}, 
                    }
                },                       
}


BLACK = '\u001b[30m'
RED = '\u001b[31m'
GREEN = '\u001b[32m'
YELLOW = '\u001b[33m'
BLUE = '\u001b[34m'
MAGENTA = '\u001b[35m'
CYAN = '\u001b[36m'
WHITE = '\u001b[37m'
RESET = '\u001b[0m'
 
BOLD = '\u001b[1m'
UNDERLINE = '\u001b[4m'
REVERSE = '\u001b[7m'


def color_print(text, effect):
    output_string = "{0}{1}{2}".format(effect, text, RESET)
    print(output_string)


class JBCompleter:
    def __init__(self, options: list):
        self.set_options(options)

    def completer(self, text, state) -> str:
        options = [o for o in self.options if o.startswith(text)]
        if state < len(options):
            return options[state]
        return None

    def get_input(self) -> str:
        print(self.options)
        readline.parse_and_bind("tab: complete")
        readline.set_completer(self.completer)
        return input('Choose: ')

    def set_options(self, options: list) -> None:
        self.options = sorted(options)

    def get_options(self) -> list:
        return self.options


print()
banner_text("*")
banner_text("Welcome to Bob's Jukebox!")
banner_text("Please enter your credit card")
banner_text(" to begin your listening journey.")
banner_text()
banner_text("Okay, I'm kidding with ya.")
banner_text("Just follow the directions below.")
banner_text("*")
print()

color_print('<->' * 20, YELLOW)
color_print("\nType Yes To Enter Bob's Jukebox", GREEN)
color_print("Type anything else, Lieutenant Dan screams at you!", GREEN)
color_print("\nType QUIT to Exit Jukebox: ", RED)

invalid_choice = True
while invalid_choice:
    enter = input("\n: ").casefold()
    if enter == 'quit':
        print("\nWalk On Home You QUITTER!")
        exit()
    if enter != 'yes':
        color_print("\nDAMMIT GUMP! Just type Yes!", RED)
    else:
        invalid_choice = False
        color_print('<->' * 20, YELLOW)   
        print("\nPick Your Vibes:")
        for key in bands: 
            color_print(key.title(), BLUE)


# Choose Band
band_choices = [band.title() for band in bands.keys()]
completer = JBCompleter(band_choices)
while True:
    pick_band = completer.get_input().lower()
    if pick_band == 'quit':
        print("\nWalk On Home You QUITTER!")
        exit()
    if pick_band not in bands.keys():
        print("Invalid Band! Read Carefully and Pick Your Vibes!")
        continue
    break

# Choose Album
print("\nChoose Album: ")
albums = [album.title() for album in bands[pick_band]['albums']]
color_print('\n' + '<->' * 20, YELLOW)
for album in albums:
    color_print(album, MAGENTA)
color_print('\n' + '<->' * 20, YELLOW)
completer.set_options(albums)
while True:
    chosen_album = completer.get_input()
    if chosen_album == 'quit':
        print("\nWalk On Home You QUITTER!")
        exit()
    if chosen_album not in albums:
        print("Invalid Album. Choose Again!")
        continue
    break

chosen_album = chosen_album.lower()
tracklist = bands[pick_band]['albums'][chosen_album]
for key, value in tracklist.items():
    if key != 'year':
        print(key + '.', value.title())
year = bands[pick_band]['albums'][chosen_album]['year']
color_print("\nFun Fact: This album was released in {}.".format(year), GREEN)   

# Choose a song
invalid_song = True
while invalid_song:
    chosen_song = input("\nEnter the tracklist number to begin listening or type Random for a surprise: ").casefold()
    if chosen_song == 'quit':
        print("\nWalk On Home You QUITTER!")
        exit()
    # import pdb; pdb.set_trace()
    if chosen_song == 'random':
        invalid_song = False
        # album_songs = random.choice(list(bands[pick_band]['albums'][chosen_album].keys()))
        # random_track_pick = bands[pick_band]['albums'][chosen_album][album_songs]
        # print("\nYou are now listening to the random track '{}' from the album '{}'".format(random_track_pick, chosen_album))
        random_song = random.choice(list(bands[pick_band]['albums'][chosen_album].keys())) # picks random `key` when drilling down dict
        random_pick = bands[pick_band]['albums'][chosen_album][random_song] # Line 98 variable is inserted and prints value
        color_print("\nYou are now listening to the random track '{}' from the album '{}'".format(random_pick.title(), chosen_album.title()), CYAN)
    elif chosen_song not in tracklist:
        print("Invalid Track. Choose Again!")    
    else:
        invalid_song = False    
        select_song =  bands[pick_band]['albums'][chosen_album][chosen_song]
        color_print("\nYou are now listening to '{}' from the album '{}'.".format(select_song.title(), chosen_album.title()), CYAN)   
